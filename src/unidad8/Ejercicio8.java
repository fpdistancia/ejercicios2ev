package unidad8;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import unidad7.Libro;
import unidad7.Publicacion;
import unidad7.Revista;

public class Ejercicio8 {

	public static void main(String[] args) {
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("res/ejercicio8.obj")));
			out.writeObject(new Libro(101, "Java 9", 2020, "Schildt, Herbert"));
			Libro l = new Libro(133, "UML Gota a Gota", 1999);
			l.agregarAutor("Martin Fowler");
			l.agregarAutor("Kendall Scott");
			out.writeObject(l);
			out.writeObject(new Revista(468, "Muy Interesante", 2020, 4, 22));
			out.writeObject(new Revista(563, "Computer Hoy", 2020, 4, 30));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (out != null)
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new BufferedInputStream(new FileInputStream("res/ejercicio8.obj")));
			boolean eof = false;
			while (!eof) {
				try {
					System.out.println((Publicacion) in.readObject());
				} catch (EOFException e) {
					eof = true;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

}
