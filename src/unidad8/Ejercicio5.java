package unidad8;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Ejercicio5 {

	public static void main(String[] args) {
		DataInputStream in = null;
		PrintWriter out = null;
		try {
			in = new DataInputStream(new BufferedInputStream(new FileInputStream("res/ejercicio4.bin")));
			out = new PrintWriter(new FileWriter("res/ejercicio5.txt"));
			boolean fin = false;
			while (!fin)
				try {
					out.println(in.readUTF() + " - " + in.readInt() + " caracteres - " +
							in.readInt() + " palabras - " + in.readInt() + " lineas");
				} catch (EOFException e) {
					fin = true;
				}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (in != null)
					in.close();
				if (out != null)
					out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
