package unidad8;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class Ejercicio2 {

	public static void main(String[] args) {
		
		/* 
		 * Es posible que al ejecutar esta primera parte se tenga la sensación de que el 
		 * programa se ha bloquedado
		 */
		FileInputStream in1 = null;
		try {
			in1 = new FileInputStream("res/El Quijote UTF8.txt");
			long t0 = System.nanoTime();
			while (in1.read() >= 0);
			System.out.println("Tiempo empleado sin buffer: " + ((double) (System.nanoTime() - t0) / 1000000000d) + " segundos"); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (in1 != null)
				try {
					in1.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		
		/* 
		 * La ejecución de esta parte debería de ser mucho más rápida
		 */
		BufferedInputStream in2 = null;
		try {
			in2 = new BufferedInputStream(new FileInputStream("res/El Quijote UTF8.txt"));
			long t0 = System.nanoTime();
			while (in2.read() >= 0);
			System.out.println("Tiempo empleado con buffer: " + ((double) (System.nanoTime() - t0) / 1000000000d) + " segundos"); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (in2 != null)
				try {
					in2.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
	}

}
