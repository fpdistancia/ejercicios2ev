package unidad8;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio6 {

	public static void main(String[] args) throws NumberFormatException, IOException {
		String[] fichero = { "res/El Quijote UTF8.TXT", "res/El Quijote Windows-1252.txt" };
		String[] codificacion = { "UTF8", "Windows-1252" };
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int opcion;
		do {
			System.out.print("Elige una opción (1 = UTF8, 2 = Windows-1252): ");
			opcion = Integer.parseInt(in.readLine()) - 1;
		} while (opcion != 0 && opcion != 1);
		mostrarArchivo(fichero[opcion], codificacion[opcion]);
	}

	static void mostrar(byte[] array) {
		for (byte b : array)
			System.out.printf("%x ", b);
		System.out.println();
	}

	static void mostrarArchivo(String fichero, String codificacion) {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(fichero), codificacion));
			String linea;
			while ((linea = in.readLine()) != null)
				System.out.println(linea);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

}
