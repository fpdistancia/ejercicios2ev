package unidad8;

import java.io.File;

public class Ejercicio1 {

	public static void main(String[] args){
		File file = new File(args[0]);
		if (!file.exists())
			System.out.println("El fichero o directorio no existe");
		else {
			if (file.isDirectory())
				System.out.println("DIRECTORIO: " + file.getName());
			else {
				System.out.println("FICHERO: " + file.getName());
				System.out.println("Tamaño: " + file.length());
				System.out.println("Permiso de ejecución: " + (file.canExecute()?"si":"no"));
				System.out.println("Permiso de lectura: " + (file.canRead()?"si":"no"));
				System.out.println("Permiso de escritura: " + (file.canWrite()?"si":"no"));
			}
		}
	}
}
