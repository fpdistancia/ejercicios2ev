package unidad8;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class Ejercicio3v3 {

	public static void main(String[] args) {
		BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
		BufferedReader in = null;
		String file;
		
		try {
			if (args.length < 1) {
				System.out.println("Ruta: ");
				file = sysin.readLine();
			}
			else
				file = args[0];
			
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "Windows-1252"));
			
			long numc = 0;
			long nump = 0;
			long numl = 0;
			int c;
			boolean palabra = false;
			
			System.out.println("Contando ...");
			long t0 = System.nanoTime();
			while ((c = in.read()) >= 0) {
				if (c != '\n' && c != '\r')
					numc++;
				if (Character.isWhitespace(c)) {
					if (palabra) {
						palabra = false;
						nump++;
					}
					if (c == '\n')
						numl++;
				}
				else if (!palabra)
					palabra = true;					
			}
			System.out.println("Número de caracteres: " + numc);
			System.out.println("Número de palabras: " + nump);
			System.out.println("Número de líneas: " + numl);
			System.out.println("Tiempo empleado: " + ((double) (System.nanoTime() - t0) / 1000000000d) + " segundos");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
}
