package unidad8;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Ejercicio3v1 {

	public static void main(String[] args) {
		BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
		BufferedReader in = null;
		long numc = 0;
		long nump = 0;
		long numl = 0;
		String file;
		
		try {
			if (args.length < 1) {
				System.out.println("Ruta: ");
				file = sysin.readLine();
			}
			else
				file = args[0];
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String linea;
			System.out.println("Contando ...");
			long t0 = System.nanoTime();
			while ((linea = in.readLine()) != null) {
				numc += linea.length();
				numl++;
				Scanner s = new Scanner(linea);
				while (s.hasNext()) {
					s.next();
					nump++;
				}
				s.close();
			}
			System.out.println("Número de caracteres: " + numc);
			System.out.println("Número de palabras: " + nump);
			System.out.println("Número de líneas: " + numl);
			System.out.println("Tiempo empleado: " + ((double) (System.nanoTime() - t0) / 1000000000d) + " segundos");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
}
