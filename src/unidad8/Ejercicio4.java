package unidad8;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
		BufferedReader in = null;
		int numc = 0;
		int nump = 0;
		int numl = 0;
		String file;
		
		try {
			if (args.length < 1) {
				System.out.println("Ruta: ");
				file = sysin.readLine();
			}
			else
				file = args[0];
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String linea;
			while ((linea = in.readLine()) != null) {
				numc += linea.length();
				numl++;
				Scanner s = new Scanner(linea);
				while (s.hasNext()) {
					s.next();
					nump++;
				}
				s.close();
			}
			escribirDatos(file, numc, nump, numl);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	static void escribirDatos(String file, int numc, int nump, int numl) {
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("res/ejercicio4.bin", true)));
			out.writeUTF(file);
			out.writeInt(numc);
			out.writeInt(nump);
			out.writeInt(numl);
			System.out.println("Datos guardados");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (out != null)
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
}
