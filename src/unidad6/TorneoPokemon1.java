package unidad6;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class TorneoPokemon1 {
	
	/* 
	 * Esta clase va implementar el desarrollo de un torneo de pokemons haciendo
	 * que la entrada de datos sea la más flexible a la hora de interpretar las
	 * especificaciones dadas en el enunciado.
	 * 
	 * El programa funcionará correctamente siempre que el usuario introduzca los datos
	 * sin cometer ninguna equivocación en el tipo y en el orden especificados, aceptando
	 * que se escriban en varias líneas datos que deberían ir en una sola línea.
	 * 
	 * No se comprobará que el nombre de los entrenadores, pokemons o elementos esté formado
	 * únicamente por caracteres alfabéticos.
	 * 
	 * El único error de entrada que hará que el programa se detenga, será introducir un
	 * valor no numérico cuando se lea la salud de un pokemon.
	 */
	
	private Map<String, Entrenador> entrenadores = new HashMap<>();
	private Scanner in = new Scanner(System.in);
	
	private void leerDatos(String nombreEntrenador) {
		Entrenador entrenador = entrenadores.get(nombreEntrenador);
		if (entrenador == null) {
			entrenador = new Entrenador(nombreEntrenador);
			entrenadores.put(nombreEntrenador, entrenador);
		}
		if (!entrenador.addPokemon(new Pokemon(in.next(), in.next(), in.nextInt())))
			System.out.println("Este entrenador ya tiene un pokemon con ese nombre");
	}
	
	private void ejecutar() {
		boolean torneo = false;
		String next;
		
		while (!(next = in.next()).trim().equalsIgnoreCase("fin")) {
			if (next.equalsIgnoreCase("torneo"))
				torneo = true;
			else if (!torneo)
				leerDatos(next);
			else
				for (Entrenador e: entrenadores.values())
					e.jugarRonda(next);
		}
		
		Set<Entrenador> listaEntrenadores = new TreeSet<>(new EntrenadorComparator());
		listaEntrenadores.addAll(entrenadores.values());
		for (Entrenador e: listaEntrenadores)
			System.out.printf("%s %d %d\n", e.getNombre(), e.getInsignias(), e.getNumPokemons());
	}
	
	public static void main(String[] args) throws IOException {
		TorneoPokemon1 torneo = new TorneoPokemon1();
		torneo.ejecutar();
	}
	
}

/*
Ash Charizard fuego 100
Brock Squirtle agua 38
Ash Pikachu electricidad 10
torneo
fuego
electricidad
fin

Misty Blastoise agua 18
Clemont Pikachu electricidad 22
Millo Kadabra psíquico 90
torneo
fuego
electricidad
fuego
fin
*/