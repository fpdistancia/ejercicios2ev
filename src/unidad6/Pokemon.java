package unidad6;

public class Pokemon implements Comparable<Pokemon> {
	private String nombre;
	private String elemento;
	private int salud;
	
	public Pokemon(String nombre, String elemento, int salud) {
		if (nombre == null || elemento == null || salud <= 0)
			throw new IllegalArgumentException();
		this.nombre = nombre;
		this.elemento = elemento;
		this.salud = salud;
	}

	public String getNombre() {
		return nombre;
	}

	public String getElemento() {
		return elemento;
	}

	public int getSalud() {
		return salud;
	}
	
	public int decSalud(int puntos) {
		salud -= puntos;
		return salud;
	}
	
	public int incSalud(int puntos) {
		salud += puntos;
		return salud;
	}

	@Override
	public String toString() {
		return "Pokemon [" + nombre + ", " + elemento + ", " + salud + "]";
	}

	/*
	 * En términos generales, el criterio de igualdad de una clase de objetos
	 * debería estar basado en el valor de todos los atributos. Sin embargo,
	 * en este caso sólo se tendrá en cuenta el nombre del pokemon para
	 * simplificar, usando un HashSet, la implementación de la restricción que
	 * prohibe que un entrenador tenga dos pokemons con el mismo nombre.
	 */	
	@Override
	public boolean equals(Object o) {
		return this == o || (o != null &&
				getClass() == o.getClass() &&
				nombre.equals(((Pokemon) o).nombre));
	}
	
	@Override
	public int hashCode() {
		return nombre.hashCode();
	}
	
	
	@Override
	public int compareTo(Pokemon pokemon) {
		return nombre.compareTo(pokemon.nombre);
	}
	
	
	
}
