package unidad6;

import java.util.Comparator;

public class EntrenadorComparator implements Comparator<Entrenador> {

	@Override
	public int compare(Entrenador e1, Entrenador e2) {
		int resultado;
		int i1 = e1.getInsignias();
		int i2 = e2.getInsignias();
		// odenación de mayor a menor
		if (i1 > i2)
			resultado = -1;
		else if (i1 < i2)
			resultado = 1;
		else
			resultado = 0;
		if (resultado == 0) {
			int p1 = e1.getNumPokemons();
			int p2 = e2.getNumPokemons();
			// odenación de mayor a menor
			if (p1 > p2)
				resultado = -1;
			else if (p1 < p2)
				resultado = 1;
			else
				resultado = 0;
			if (resultado == 0)
				resultado = e1.getNombre().compareTo(e2.getNombre());
		}
		return resultado;
	}

}
