package unidad6;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Entrenador implements Comparable<Entrenador> {
	
	private String nombre;
	private int insignias = 0;
	private Set<Pokemon> pokemons = new TreeSet<Pokemon>();
	
	public Entrenador(String nombre) {
		if (nombre == null)
			throw new IllegalArgumentException();
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}
	
	public int getInsignias() {
		return insignias;
	}
	
	public int getNumPokemons() {
		return pokemons.size();
	}

	public boolean addPokemon(Pokemon pokemon) {
		return pokemons.add(pokemon);
	}
	
	private boolean penaliza(String elemento) {
		boolean penaliza = true;
		Iterator<Pokemon> i = pokemons.iterator();
		while (penaliza && i.hasNext())
			penaliza = !i.next().getElemento().equalsIgnoreCase(elemento);
		return penaliza;
	}
	
	public void jugarRonda(String elemento) {
		if (penaliza(elemento)) {
			Iterator<Pokemon> i = pokemons.iterator();
			while (i.hasNext()) {
				Pokemon pokemon = i.next();
				if (pokemon.decSalud(10) <= 0)
					i.remove();
			}
		}
		else
			insignias++;
	}

	@Override
	public String toString() {
		return "Entrenador " + nombre + ", " + insignias + " " + pokemons.toString();
	}

	/*
	 * Generado automáticamente por Eclipse.
	 */	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + insignias;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((pokemons == null) ? 0 : pokemons.hashCode());
		return result;
	}

	/*
	 * Generado automáticamente por Eclipse.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entrenador other = (Entrenador) obj;
		if (insignias != other.insignias)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (pokemons == null) {
			if (other.pokemons != null)
				return false;
		} else if (!pokemons.equals(other.pokemons))
			return false;
		return true;
	}

	@Override
	public int compareTo(Entrenador entrenador) {
		return nombre.compareTo(entrenador.nombre);
	}

}
