package unidad6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

public class TorneoPokemon2 {
	
	/* 
	 * Esta clase va implementar el desarrollo de un torneo de pokemons con una entrada
	 * de datos menos flexible con respecto a las especificaciones dadas en el enunciado.
	 * Se permitirán espacios en blanco al principio y al final de cada línea, pero no se
	 * permitirá repartir en varias líneas datos que deberían de escribirse en una sóla línea.
	 * 
	 * Tampoco se permitirá la introducción de más datos de los esperados en una línea.
	 * 
	 * Se utilizan expresiones regulares para comprobar que los nombres de entrenadores,
	 * pokemons y elementos están formados únicamente por caracteres alfabéticos.
	 *  
	 * Si se procuce algún error, se informará del mismo al usuario mediante mensajes en la
	 * consola y se leerá la siguiente línea.
	 * 
	 * Los mensajes de error serán genéricos, es decir, se verá el mismo mensaje de error
	 * si no se utilizan caracteres alfabéticos en un nombre que si no se introduce un valor
	 * númerico para la salud de un pokemon.
	 */
	
	private Map<String, Entrenador> entrenadores = new TreeMap<>();
	
	private void guardarDatos(String linea) {
		Scanner s = null;
		try {
			s = new Scanner(linea);
			String nombreEntrenador =  s.next("\\p{L}+");
			String nombrePokemon = s.next("\\p{L}+");
			String elemento = s.next("\\p{L}+");
			int salud = Integer.parseInt(s.next("\\d+$"));
			/*
			 * la sentencia anterior se puede sustituir por las dos siguientes:
			 *  int salud = s.nextInt();
			 *  if (s.hasNext())
			 *  	throw new InputMismatchException();
			 */
			Entrenador entrenador = entrenadores.get(nombreEntrenador);
			if (entrenador == null) {
				entrenador = new Entrenador(nombreEntrenador);
				entrenadores.put(nombreEntrenador, entrenador);
			}
			if (!entrenador.addPokemon(new Pokemon(nombrePokemon, elemento, salud)))
				System.out.println("Este entrenador ya tiene un pokemon con ese nombre");
		} catch (NoSuchElementException e) {
			System.out.println("Error: datos de entrada incorrectos");
		} finally {
			s.close();
		}
	}
	
	private boolean ronda(String elemento) {
		if (Pattern.matches("\\p{L}+", elemento)) {
			for (Entrenador e: entrenadores.values())
					e.jugarRonda(elemento);
			return false;
		}
		else {
			System.out.println("Error: datos de la ronda incorrectos");
			return true;
		}	
	}
	
	private void ejecutar() throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		boolean torneo = false;
		String linea;
		
		while (!(linea = in.readLine().trim()).equalsIgnoreCase("fin")) {
			if (linea.equalsIgnoreCase("torneo"))
				torneo = true;
			else if (torneo)
				ronda(linea);
			else
				guardarDatos(linea);
		}
		Set<Entrenador> listaEntrenadores = new TreeSet<>(new EntrenadorComparator());
		listaEntrenadores.addAll(entrenadores.values());
		for (Entrenador e: listaEntrenadores)
			System.out.printf("%s %d %d\n", e.getNombre(), e.getInsignias(), e.getNumPokemons());
	}
	
	public static void main(String[] args) throws IOException {
		TorneoPokemon2 torneo = new TorneoPokemon2();
		torneo.ejecutar();
	}
	
}

/*
Ash Charizard fuego 100
Brock Squirtle agua 38
Ash Pikachu electricidad 10
torneo
fuego
electricidad
fin

Misty Blastoise agua 18
Clemont Pikachu electricidad 22
Millo Kadabra psíquico 90
torneo
fuego
electricidad
fuego
fin
*/