package unidad6.exepciones;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Ejercicio2 {

	public static double [] esg( double a, double b, double c) {
		if (a == 0)
			throw new IllegalArgumentException();
		double d = b * b - 4 * a * c;
		if (d < 0)
			throw new ArithmeticException();
		double r = Math.sqrt(d);
		double [] soluciones = new double[2];
		soluciones[0] = (-b + r) / (2 * a);
		soluciones[1] = (-b - r) / (2 * a);
		return soluciones;
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Coeficientes: ");
		try {
			double [] soluciones = esg(in.nextDouble(), in.nextDouble(), in.nextDouble());
			System.out.printf("Soluciones: %s", Arrays.toString(soluciones));
		} catch (IllegalArgumentException e) {
			System.out.println("no es una ecuación de segundo grado");
		} catch (ArithmeticException e) {
			System.out.println("la ecuación no tiene una solución real");
		} catch (InputMismatchException e) {
			System.out.println("alguno de los coeficientes no es un número");
		}
		in.close();
	}

}
