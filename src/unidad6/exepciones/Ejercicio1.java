package unidad6.exepciones;

public class Ejercicio1 {

	public static boolean isInt(String n) {
		try {
			Integer.parseInt(n);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public static boolean isDouble(String n) {
		try {
			Double.parseDouble(n);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(isInt("123"));
		System.out.println(isInt("12.3"));
		System.out.println(isInt("abc"));
		System.out.println(isDouble("123"));
		System.out.println(isDouble("12.3"));
		System.out.println(isDouble("abc"));
	}

}
