package unidad6.exepciones;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio3 {

	static double cateto(String s) {
		double c = Double.parseDouble(s);
		if (c < 0)
			throw new IllegalArgumentException();
		return c;
	}
	
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		double a = 0;
		double b = 0;
		double h;
		String linea;
		
		System.out.print("> ");
		while (!(linea = in.readLine().trim()).equalsIgnoreCase("f")) {
			String [] cmd = linea.split("\\s+");
			try {
				if (cmd.length == 2 && cmd[0].equalsIgnoreCase("a"))
					a = cateto(cmd[1]);
				else if (cmd.length == 2 && cmd[0].equalsIgnoreCase("b"))
					b = cateto(cmd[1]);
				else if (cmd.length == 1 && cmd[0].equalsIgnoreCase("c")) {
					h = Math.sqrt(a * a + b * b);
					System.out.printf("a = %.5f, b = %.5f, h = %.5f\n", a, b, h);
					a = b = 0;
				}
				else {
					if (linea.length() > 10)
						linea = linea.substring(0, 10) + "...";
					System.out.printf("'%s' no es un comando válido\n", linea);
				}
			} catch (NumberFormatException e) {
				System.out.printf("'%s' no es un valor numérico\n> ", cmd[1]);
			} catch (IllegalArgumentException e) {
				System.out.printf("'%s' no es un valor válido para el cateto\n", cmd[1]);
			}
			System.out.print("> ");
		}
	}

}
