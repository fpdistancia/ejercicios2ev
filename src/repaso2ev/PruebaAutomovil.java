package repaso2ev;

import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class PruebaAutomovil {

	public static void main(String[] args) {
		/*
		 * Para simplificar el problema, se da por hecho que los datos de entrada se ajustan
		 * siempre al formato especificado, y por tanto no será necesario llevar a cabo ninguna
		 * comprobación de errores.
		 */
		Scanner in = new Scanner(System.in);
		in.useLocale(Locale.US);
		Map<String, Automovil> automoviles = new TreeMap<>();
		int n = in.nextInt();
		for (int i=0; i<n; i++) {
			String modelo = in.next();
			automoviles.put(modelo, new Automovil(modelo, in.nextFloat(), in.nextFloat(), in.nextFloat()));
		}
		
		while (!in.next().equalsIgnoreCase("fin"))  {
			String modelo = in.next();
			float kms = in.nextFloat();
			Automovil automovil = automoviles.get(modelo);
			float consumido = automovil.desplazar(kms);
			if (consumido > 0)
				System.out.printf(Locale.US, "%s %.2f %.2f\n", modelo, automovil.getLitrosDeposito(), consumido);
			else
				System.out.println("Combustible insuficiente para este desplazamiento");
		}
		System.out.println("-----------------------------------");
		automoviles.forEach((modelo, automovil) -> {
			System.out.printf(Locale.US, "%s %.2f %.2f %.2f\n", modelo, automovil.getLitrosDeposito(),
					automovil.getTotalKms(), automovil.getTotalConsumo());
		});
		in.close();
	}

/*
2
AudiA4 80 23 0.3
BMW-M2 75 45 0.42
desplazar AudiA4 5
desplazar BMW-M2 56
desplazar AudiA4 13
fin 
*/
	
/*
3
AudiA4 80 18 0.34
BMW-M2 75 33 0.41
Ferrari-488Spider 70 50 0.47
desplazar Ferrari-488Spider 97
desplazar AudiA4 85
desplazar Ferrari-488Spider 35
desplazar AudiA4 50
fin  
*/
	
	
}
