package repaso2ev;

public class Matrices {

	public static int [][] cuadrada1(int dim) {
		int n = 1;
		int [][] matriz = new int[dim][dim];
		for (int c=0; c<dim; c++)
			for (int f=0; f<dim; f++)
				matriz[f][c] = n++;
		return matriz;
	}
	
	public static int [][] cuadrada2(int dim) {
		int n = 1;
		int [][] matriz = new int[dim][dim];
		for (int c=0; c<dim; c++) {
			if (c % 2 == 0)
				for (int f=0; f<dim; f++)
					matriz[f][c] = n++;
			else
				for (int f=dim-1; f>=0; f--)
					matriz[f][c] = n++;
		}
		return matriz;
	}
	
	public static String [][] palindromos(int c, int f) {
		if (c > 26)
			c = 26;
		else if (c < 1)
			c = 1;
		
		if (f > 26)
			f = 26;
		else if (f < 1)
			f = 1;
		
		String [][] matriz = new String[f][c];
		
		for (int i=0; i<f; i++)
			for (int j=0; j<c; j++) {
				int d = i + j;
				char m = d >= 26 ? (char) ('A' + (d % 26)) : (char) ('a' + d); 
				matriz[i][j] = String.valueOf((char) ('a' + i)) + String.valueOf(m) + String.valueOf((char) ('a' + i));
			}
		
		return matriz;
	}
 
	
	public static int max3x3sum(int [][] matriz) {
		if (matriz.length < 3)
			throw new IllegalArgumentException();
		for (int i=0; i<matriz.length; i++)
			if (matriz[i].length < 3)
				throw new IllegalArgumentException();
		int suma;
		int max = Integer.MIN_VALUE;
		for (int i=0; i<matriz.length-2; i++)
			for (int j=0; j<matriz[i].length-2; j++) {
				suma = 0;
				for (int k=i; k<i+3; k++)
					for (int l=j; l<j+3; l++)
						suma += matriz[k][l];
				if (suma > max) max = suma;
			}
		return max;
	}

	
}
