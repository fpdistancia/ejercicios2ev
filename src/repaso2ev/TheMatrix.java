package repaso2ev;

import java.util.Arrays;
import java.util.Scanner;

public class TheMatrix {

	static void relleno(char [][] m, int f, int c, char relleno) {
		char inicial = m[f][c];
		m[f][c] = relleno;
		if (f > 0 && m[f-1][c] == inicial)
			relleno(m, f-1, c, relleno);
		if (f < m.length-1 && m[f+1][c] == inicial)
			relleno(m, f+1, c, relleno);
		if (c > 0 && m[f][c-1] == inicial)
			relleno(m, f, c-1, relleno);
		if (c < m[f].length-1 && m[f][c+1] == inicial)
			relleno(m, f, c+1, relleno);
	}
	
	static void mostrarMatriz(char [][] matriz) {
		for (int i=0; i<matriz.length; i++)
			System.out.println(Arrays.toString(matriz[i]));
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int f = in.nextInt();
		int c = in.nextInt();
		char[][] matriz = new char[f][c];
		for (int i=0; i<f; i++)
			for (int j=0; j<c; j++)
				matriz[i][j] = in.next().charAt(0);
		char relleno = in.next().charAt(0);
		mostrarMatriz(matriz);
		System.out.println("-----------------------");
		relleno(matriz, in.nextInt(), in.nextInt(), relleno);
		mostrarMatriz(matriz);
		in.close();
	}
	
/*
5 6
o o 1 1 o o
o 1 o o 1 o
1 o o o o 1
o 1 o o 1 o
o o 1 1 o o
3
2 1
*/

}

