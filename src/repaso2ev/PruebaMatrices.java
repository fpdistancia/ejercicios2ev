package repaso2ev;

public class PruebaMatrices {

	public static void mostrarMatriz(int [][] matriz) {
		for (int f=0; f<matriz.length; f++) {
			for (int c=0; c<matriz[f].length; c++)
				System.out.print(matriz[f][c] + " ");
			System.out.println();
		}
	}
	
	public static void mostrarMatriz(String [][] matriz) {
		for (int f=0; f<matriz.length; f++) {
			for (int c=0; c<matriz[f].length; c++)
				System.out.print(matriz[f][c] + " ");
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		mostrarMatriz(Matrices.cuadrada1(5));
		System.out.println("-----------------------------------");
		mostrarMatriz(Matrices.cuadrada2(5));
		System.out.println("-----------------------------------");
		mostrarMatriz(Matrices.palindromos(26, 26));
		int [][] matriz = {
			{1, 5, 5, 2, 4},   
			{2, 1, 4, 14, 3},
			{3, 7, 11, 2, 8}, 
			{4, 8, 12, 16, 4}   
		};
		System.out.println("-----------------------------------");
		System.out.println(Matrices.max3x3sum(matriz));
	}
	
}
