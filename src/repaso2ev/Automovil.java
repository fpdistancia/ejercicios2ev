package repaso2ev;

public class Automovil {
	
	private String modelo;
	private float capacidadDeposito;
	private float litrosDeposito;
	private float consumoKm;
	private float totalKms;
	private float totalConsumo;
	
	public Automovil(String modelo, float capacidadDeposito, float litrosDeposito, float consumoKm) {
		this.modelo = modelo;
		this.capacidadDeposito = capacidadDeposito;
		this.litrosDeposito = litrosDeposito;
		this.consumoKm = consumoKm;
	}

	public Automovil(String modelo, float capacidadDeposito, float consumoKm) {
		this.modelo = modelo;
		litrosDeposito = this.capacidadDeposito = capacidadDeposito;
		this.consumoKm = consumoKm;
	}
	
	public void llenarDeposito() {
		litrosDeposito = capacidadDeposito;
	}
	
	public float llenarDeposito(float litros) {
		if (litros <= 0)
			return 0;
		float sobrante = 0;
		litrosDeposito += litros;
		if (litrosDeposito > capacidadDeposito) {
			sobrante = litrosDeposito - capacidadDeposito;
			litrosDeposito = capacidadDeposito;
		}
		return sobrante;
	}
	
	public float desplazar(float kms) {
		float consumo = kms * consumoKm;
		if (consumo > litrosDeposito)
			return 0;
		totalKms += kms;
		totalConsumo += consumo;
		litrosDeposito -= consumo;
		return consumo;
	}

	public String getModelo() {
		return modelo;
	}

	public float getCapacidadDeposito() {
		return capacidadDeposito;
	}

	public float getLitrosDeposito() {
		return litrosDeposito;
	}

	public float getConsumoKm() {
		return consumoKm;
	}

	public float getTotalKms() {
		return totalKms;
	}

	public float getTotalConsumo() {
		return totalConsumo;
	}
	
	public void reset() {
		totalConsumo = totalKms = 0;
	}
	
}
