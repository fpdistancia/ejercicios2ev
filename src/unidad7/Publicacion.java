package unidad7;

import java.io.Serializable;

public abstract class Publicacion implements Comparable<Publicacion>, Serializable {
	
	private static final long serialVersionUID = 42721822971688462L;
	private int codigo;
	private String titulo;
	private int año;
	
	public Publicacion(int codigo, String titulo, int año) {
		this.codigo = codigo;
		this.titulo = titulo;
		this.año = año;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public int getAño() {
		return año;
	}

	@Override
	public String toString() {
		return "[" + codigo + ", " + titulo + ", " + año + "]";
	}
	
	@Override
	public final int compareTo(Publicacion p) {
		return getTitulo().compareTo(p.getTitulo());
	}
	
}
