package unidad7;

import java.util.Comparator;

public class LibroComparator implements Comparator<Libro> {

	@Override
	public int compare(Libro l1, Libro l2) {
		int resultado = l1.getAutores().toString().compareTo(l2.getAutores().toString());
		return resultado != 0 ? resultado : l1.getTitulo().compareTo(l2.getTitulo());
	}

}
