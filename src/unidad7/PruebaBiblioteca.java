package unidad7;

import java.util.Iterator;
import java.util.TreeSet;

public class PruebaBiblioteca {
	
	public static void main(String[] args) {
		/*
		 * El polimorfismo comienza a ponerse de manifiesto al alamcenar diferentes clases de
		 * publicaciones en una misma estructura de datos:
		 */
		
		Libro l1 = new Libro(101, "Java 9", 2020, "Schildt, Herbert");
		Libro l2 = new Libro(133, "UML Gota a Gota", 1999);
		l2.agregarAutor("Martin Fowler");
		l2.agregarAutor("Kendall Scott");
		TreeSet<Publicacion> publicaciones = new TreeSet<>();
		publicaciones.add(new Revista(468, "Muy Interesante", 2020, 4, 22));
		publicaciones.add(l1);
		publicaciones.add(new Revista(563, "Computer Hoy", 2020, 4, 30));
		publicaciones.add(l2);
		Publicacion publicacion;
		Iterator<Publicacion> i = publicaciones.iterator();
		while (i.hasNext()) {
			/*
			 * En las dos sentencias siguientes se pone de manifiesto el polimorfismo al manejar con una
			 * única referencia polimórfica (publicacion) diferentes tipos de publicaciones:
			 */
			publicacion = i.next();
			System.out.println(publicacion);
		}
		System.out.println();
		System.out.println("Ordenados por autor: ");
		TreeSet<Libro> libros = new TreeSet<>(new LibroComparator());
		libros.add(l1);
		libros.add(l2);
		for (Libro l: libros)
			System.out.println(l + " " + l.getAutores());
	}

}
