package unidad7;

public abstract class Prestable extends Publicacion {

	private static final long serialVersionUID = -2758175631620836503L;
	private boolean prestado;
	
	public Prestable(int codigo, String titulo, int año) {
		super(codigo, titulo, año);
		prestado = false;
	}
	
	public Prestable(int codigo, String titulo, int año, boolean prestado) {
		super(codigo, titulo, año);
		this.prestado = prestado;
	}

	public boolean estaPrestado() {
		return prestado;
	}
	
	public void setPrestado(boolean prestado) {
		this.prestado = prestado;
	}
	
}
