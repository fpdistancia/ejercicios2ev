package unidad7;

public class Revista extends Publicacion {

	private static final long serialVersionUID = -4941355148083533216L;
	private int mes;
	private int dia;
	
	public Revista(int codigo, String titulo, int año, int mes, int dia) {
		super(codigo, titulo, año);
		this.mes = mes;
		this.dia = dia;
	}

	public int getMes() {
		return mes;
	}

	public int getDia() {
		return dia;
	}

	@Override
	public String toString() {
		return "Revista " + super.toString();
	}
	
}
