package unidad7;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

public class Libro extends Prestable {

	private static final long serialVersionUID = -5325076665563789383L;
	private Set<String> autores = new TreeSet<>();
	
	public Libro(int codigo, String titulo, int año, Collection<String> autores) {
		super(codigo, titulo, año);
		this.autores.addAll(autores);
	}

	public Libro(int codigo, String titulo, int año, String autor) {
		super(codigo, titulo, año);
		autores.add(autor);
	}
	
	public Libro(int codigo, String titulo, int año) {
		super(codigo, titulo, año);
	}
	
	public boolean agregarAutor(String autor) {
		return autores.add(autor);
	}
	
	public boolean eliminarAutor(String autor) {
		return autores.remove(autor);
	}
	
	public String getAutores() {
		return autores.toString();
	}

	@Override
	public String toString() {
		return "Libro " + super.toString();
	}

	
	
}
