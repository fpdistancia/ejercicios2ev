package unidad4;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int a = 0, e = 0, i = 0, o = 0, u = 0;
		String s;
		System.out.println("Introduce un texto: ");
		s = in.nextLine().toLowerCase();
		for (int j=0; j<s.length(); j++) {
			char c = s.charAt(j);
			switch (c) {
			case 'a':
				a++;
				break;
			case 'e':
				e++;
				break;
			case 'i':
				i++;
				break;
			case 'o':
				o++;
				break;
			case 'u':
				u++;
				break;
			}
		}
		System.out.println("a: " + a);
		System.out.println("e: " + e);
		System.out.println("i: " + i);
		System.out.println("o: " + o);
		System.out.println("u: " + u);
		in.close();
	}

}
