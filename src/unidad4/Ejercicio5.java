package unidad4;

import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		char [] letras = {
			'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 
			'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'
		};
		System.out.print("NIF: ");
		String nif = in.nextLine();
		int numero = Integer.valueOf(nif.substring(0, nif.length() - 1));
		char letra = nif.charAt(nif.length() - 1);
		if (letras[numero % 23] == letra)
			System.out.println("NIF correcto");
		else
			System.out.println("NIF incorrecto");
		in.close();
	}

}
