package unidad4;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		Random r = new Random();
		Scanner in = new Scanner(System.in);
		int [] c = {0, 0, 0, 0, 0, 0};
		System.out.print("Número de lanzamientos: ");
		int n = in.nextInt();
		for (int i=0; i<n; i++)
			c[r.nextInt(6)]++;
		for (int i=0; i<c.length; i++)
			System.out.printf("El %d ha salido %d veces\n", i + 1, c[i]);
		in.close();
	}

}
