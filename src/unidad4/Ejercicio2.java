package unidad4;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Cadena: ");
		String cadena = in.nextLine();
		for (int i=cadena.length()-1; i>=0; i--)
			System.out.print(cadena.charAt(i));
		in.close();
	}

}
